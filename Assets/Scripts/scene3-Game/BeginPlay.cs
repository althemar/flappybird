﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeginPlay : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameState.Instance.gameState = GameState.GameStateEnum.GetReady;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("space"))
        {
            GameState.Instance.gameState = GameState.GameStateEnum.Playing;

            GameObject.FindGameObjectWithTag("bird").AddComponent<TouchAction>();
            GameObject.FindGameObjectWithTag("bird").GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
           
            Destroy(GameObject.FindGameObjectWithTag("getReadySprites"));
        }
    }


}
