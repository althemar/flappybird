﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitBirdColor : MonoBehaviour {

	// Use this for initialization
	void Start () {
        string color = PlayerPrefs.GetString("birdColor");

        if (color == "")
        {
            color = "green";
        }

        Debug.Log(color);

        //GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Textures/" + color + "Bird1");
        
        GetComponent<Animator>().Stop();
        GetComponent<Animator>().Play(color + "BirdFlying");
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
