﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchAction : MonoBehaviour
{

    private float _impulse;
    private float _impulseAngle;

    // Use this for initialization
    void Start()
    {
        _impulse = 5;
        _impulseAngle = 45;

        GetComponent<Rigidbody2D>().angularVelocity = -70;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space") || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, _impulse);
            GetComponent<Rigidbody2D>().MoveRotation(_impulseAngle);

            SoundState.Instance.playImpulse();
        }


        Vector3 bottomLeftCorner = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Vector3 topLeftCorner = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));

        float spriteSizeY = GetComponent<SpriteRenderer>().bounds.size.y;

        if (transform.position.y + spriteSizeY / 2 > topLeftCorner.y)
        {
            Vector3 tmpPos = transform.position;
            tmpPos.y = topLeftCorner.y - spriteSizeY / 2;
            transform.position = tmpPos;
        }
        else if (transform.position.y - spriteSizeY / 2 < bottomLeftCorner.y)
        {
            gameObject.AddComponent<EndAction>();
        }

    }
}
