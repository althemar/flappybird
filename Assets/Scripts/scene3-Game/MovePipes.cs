﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePipes : MonoBehaviour {

    public GameObject pipe1Top;
    public GameObject pipe2Bottom;
    public GameObject scoreArea;

    public float scrollingSpeed;

    private float pipe1TopOriginalY;
    private float pipe2TopOriginalY;
    private float scoreAreaOriginalY;

	// Use this for initialization
	void Start () {
        pipe1TopOriginalY = pipe1Top.transform.position.y;
        pipe2TopOriginalY = pipe2Bottom.transform.position.y;
        scoreAreaOriginalY = scoreArea.transform.position.y;

    }

    // Update is called once per frame
    void Update () {

        if (GameState.Instance.gameState == GameState.GameStateEnum.GetReady)
        {
            return;
        }

        pipe1Top.GetComponent<Rigidbody2D>().velocity = new Vector2(-scrollingSpeed, 0);
        pipe2Bottom.GetComponent<Rigidbody2D>().velocity = new Vector2(-scrollingSpeed, 0);
        scoreArea.GetComponent<Rigidbody2D>().velocity = new Vector2(-scrollingSpeed, 0);


        Vector3 bottomLeftCorner = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));

        float spriteSizeX = pipe1Top.GetComponent<SpriteRenderer>().bounds.size.x;

        if (pipe1Top.transform.position.x + spriteSizeX < bottomLeftCorner.x)
        {
            Vector3 bottomRightCorner = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
            float posX = bottomRightCorner.x + spriteSizeX;

            float randomY = Random.Range(-2, 3);
            float posY;
            Vector3 tmpPos;

            posY = pipe1TopOriginalY + randomY;
            tmpPos = new Vector3(posX, posY, pipe1Top.transform.position.z);
            pipe1Top.transform.position = tmpPos;

            posY = pipe2TopOriginalY + randomY;
            tmpPos = new Vector3(posX, posY, pipe2Bottom.transform.position.z);
            pipe2Bottom.transform.position = tmpPos;

            posY = scoreAreaOriginalY + randomY;
            tmpPos = new Vector3(posX, posY, scoreArea.transform.position.z);
            scoreArea.transform.position = tmpPos;
        }

    }
}
