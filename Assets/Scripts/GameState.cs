﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameState : MonoBehaviour
{

    public static GameState Instance;

    public enum GameStateEnum
    {
        Initialization,
        MainMenu,
        GetReady,
        Playing,
        GameOver
    }

    private int score;
    public GameStateEnum gameState;

    // Use this for initialization
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(Instance.gameObject);

            score = 0;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public int GetScore()
    {
        return score;
    }

    public void AddScore(int toAdd)
    {
        score += toAdd;
        GameObject.FindGameObjectWithTag("scoreGame").GetComponent<Text>().text = score.ToString();
    }

    public void ResetScore()
    {
        score = 0;
    }

    public void LoadLevel(string name)
    {
        SceneManager.LoadScene(name);
    }



}
