﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ClickButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void onClickPlay()
    {
        GameState.Instance.LoadLevel("Scene3-Game");
    }

    public void onClickChooseBird()
    {
        GameState.Instance.LoadLevel("Scene5-ChooseBird");
    }
}
