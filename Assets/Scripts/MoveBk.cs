﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBk : MonoBehaviour {

    public float scrollingSpeed;
    public float positionRestartX;

    public bool scrollsLeft;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        if (scrollsLeft)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-scrollingSpeed, 0);
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(scrollingSpeed, 0);
        }

        Vector3 bottomLeftCorner = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Vector3 bottomRightCorner = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));

        float spriteSizeX = GetComponent<SpriteRenderer>().bounds.size.x;

        if ( (scrollsLeft && transform.position.x + spriteSizeX / 2 < bottomLeftCorner.x) || !scrollsLeft && transform.position.x - spriteSizeX / 2 > bottomRightCorner.x)
        {
            transform.position = new Vector3(positionRestartX, transform.position.y, transform.position.z);
        }
    }
}
