﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Wait2Sec : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Invoke("GoScene2", 2.0f);
	}

    private void GoScene2()
    {
        GameState.Instance.LoadLevel("Scene2-Menu");
    }
}
