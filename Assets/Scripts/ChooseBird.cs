﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseBird : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void onClickGreen()
    {
        PlayerPrefs.SetString("birdColor", "green");
        GameState.Instance.LoadLevel("Scene2-Menu");
    }

    public void onClickPurple()
    {
        PlayerPrefs.SetString("birdColor", "purple");
        GameState.Instance.LoadLevel("Scene2-Menu");
    }

    public void onClickBlue()
    {
        PlayerPrefs.SetString("birdColor", "blue");
        GameState.Instance.LoadLevel("Scene2-Menu");
    }
}
